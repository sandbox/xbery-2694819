<?php
/**
 * @file
 * The admin form definition and processing.
 */

/**
 * Implements hook_settings_form().
 */
function silktide_cc_settings_form($form, &$form_state) {
  $form['group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Silktide configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['group']['silktide_cc_theme'] = array(
    '#title' => 'Style',
    '#type' => 'select',
    '#options' => array(
      'light-floating' => t('Light floating'),
      'light-top' => t('Light top'),
      'light-bottom' => t('Light bottom'),
      'dark-top' => t('Dark top'),
      'dark-bottom' => t('Dark bottom'),
      'dark-floating' => t('Dark floating'),
      'dark-floating-tada' => t('Dark floating tada'),
      'dark-inline' => t('Dark inline'),
    ),
    '#states' => array(
      'disabled' => array(
        ':input[name="silktide_cc_theme_custom"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('Defaults to light-floating'),
    '#default_value' => variable_get('silktide_cc_theme', SILKTIDE_CC_THEME),
  );

  $form['group']['silktide_cc_dismiss'] = array(
    '#title' => 'Dismiss button text',
    '#description' => t('The dismiss button text, defaults to "Got it!".'),
    '#type' => 'textfield',
    '#default_value' => variable_get('silktide_cc_dismiss', SILKTIDE_CC_DISMISS),
  );

  $form['group']['silktide_cc_banner_text'] = array(
    '#title' => 'Banner text',
    '#description' => t('A cookie consent string to appear on the banner.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('silktide_cc_banner_text', SILKTIDE_CC_BANNER_TEXT),
  );

  $form['group']['silktide_cc_consent_link_label'] = array(
    '#title' => 'Banner link label',
    '#description' => t('A label for your banner link, defaults to "More info"'),
    '#type' => 'textfield',
    '#default_value' => variable_get('silktide_cc_consent_link_label', SILKTIDE_CC_CONSENT_LINK_LABEL),
  );

  $form['group']['silktide_cc_consent_link'] = array(
    '#title' => 'Banner link',
    '#description' => t('A link to your cookie consent page information, only URL will do'),
    '#type' => 'textarea',
    '#default_value' => variable_get('silktide_cc_consent_link', SILKTIDE_CC_CONSENT_LINK),
  );

  $form['group']['silktide_cc_consent_link_target'] = array(
    '#title' => 'Target',
    '#type' => 'select',
    '#options' => array(
      '_self' => t('_self'),
      '_blank' => t('_blank'),
      '_parent' => t('_parent'),
      '_top' => t('_top'),
    ),
    '#description' => t('Custom expiry period in days - default 365'),
    '#default_value' => variable_get('silktide_cc_consent_link_target', SILKTIDE_CC_CONSENT_LINK_TARGET),
  );

  $form['group']['silktide_cc_consent_expiry'] = array(
    '#title' => 'Expiry',
    '#type' => 'textfield',
    '#description' => t('Custom expiry period in days - default 365'),
    '#default_value' => variable_get('silktide_cc_consent_expiry', SILKTIDE_CC_CONSENT_EXPIRY),
  );
  // Advanced settings from section.
  $form['group']['geeky'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['group']['geeky']['silktide_cc_defer'] = array(
    '#title' => 'Defer Silktide CC',
    '#type' => 'checkbox',
    '#description' => t('Includes & inits Silktide CC before body end, experimental, use with caution'),
    '#default_value' => variable_get('silktide_cc_defer', SILKTIDE_CC_CONSENT_DEFER),
  );

  $form['group']['geeky']['silktide_cc_theme_custom'] = array(
    '#title' => 'Use my own styles',
    '#type' => 'checkbox',
    '#description' => t('If you want to use your own styles'),
    '#default_value' => variable_get('silktide_cc_theme_custom', SILKTIDE_CC_THEME_CUSTOM),
  );

  $form['group']['geeky']['silktide_cc_theme_custom_path'] = array(
    '#title' => 'Use my own styles',
    '#type' => 'textfield',
    '#states' => array(
      'visible' => array(
        ':input[name="silktide_cc_theme_custom"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t("If you want to use your own styles, just provide the path relative to your website's root"),
    '#default_value' => variable_get('silktide_cc_theme_custom_path', SILKTIDE_CC_THEME_CUSTOM_PATH),
  );
  $form['group']['geeky']['silktide_cc_theme_container'] = array(
    '#title' => 'Custom container class',
    '#type' => 'textfield',
    '#description' => t('The element you want the Cookie Consent notification to be appended to.'),
    '#default_value' => variable_get('silktide_cc_theme_container', SILKTIDE_CC_THEME_CUSTOM_CONTAINER),
  );

  $form = system_settings_form($form);

  return $form;
}
