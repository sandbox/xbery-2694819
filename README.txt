CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

Silktide Cookie Consent, also known as Silktide CC,
is a simple JS library which deals
with the EU Directive on Privacy and Electronic Communications
that comes into effect in the UK on 26th May 2012.

 * For a full description of the module, visit the project page:
   <url>

 * To submit bug reports and feature suggestions, or to track changes:
   <url>

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

Navigate to admin/config/system/silktidecc
to see all module configuration options.
For permission configuration consult
admin/people/permissions#module-silktide_cc.

TROUBLESHOOTING
---------------

 * If the cookie consent banner does not appear, check the following:

   - Permission configuration

   - Caches - try clearing you caches

   - Is cookieconsent.min.js present?

   - Does console in browser contain any JS errors?

MAINTAINERS
-----------

Current maintainers:
 * Lukas Beranek (xbery) - https://www.drupal.org/user/745102
